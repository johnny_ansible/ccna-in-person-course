# Day 2 Labs

## This lab will cover the basics of VLANs and go over how a MAC Address table is built.

## Part 1

1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 1 - Switching Fundamentals - VLAN - MAC Table" and start your all your nodes. ![startnodes](/day%202%20lab%201%20image001.png)
2. Check the MAC-Address table of SW1 & SW2
3. Ping from PC1 to PC6
4. Predict, then check the SW1 & SW2 MAC-Address Table
5. Get all PC's onto both Switches MAC-Address Tables


## Part 2
1. Create and name VLAN 10,20,30 on SW1 and SW2
2. Verify VLAN creation using a show command
3. Configure each port in the correct mode and vlan
4. Verify which ports are in which VLAN with a show command
5. Try and ping between PC2 and PC5. Why doesn't it work?
6. Create a trunk port between both switches
7. Verify ports are trunks using a show command
8. Verify connectivity only between devices in the same VLAN
