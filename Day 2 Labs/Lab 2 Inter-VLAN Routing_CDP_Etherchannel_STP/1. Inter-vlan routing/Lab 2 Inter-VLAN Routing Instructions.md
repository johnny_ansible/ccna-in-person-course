# Day 2 Lab 2 Inter-VLAN Routing Lab instructions

1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 2 - Inter-VLAN Routing - Etherchannel - CDP - STP" and start your all your nodes. ![startnodes](/Day%202%20lab%202%20image001.png)
2. Create VLAN 10,20 and 30 on SW3
3. Configure all of SW3's ports as trunks, SW1/SW2 sides as well.
4. Verify VLANs and Trunk ports via show commands on SW3
5. Turn on R1 port e0/0
6. Configure a subinterface for each VLAN
7. Confirm the port on SW3 facing R1 is a trunk port
8. Verify full connectivity between all PC's