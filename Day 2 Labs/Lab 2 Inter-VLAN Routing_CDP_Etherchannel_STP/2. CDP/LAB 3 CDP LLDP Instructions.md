# Day 2 La 3 CDP LLDP Instruction

1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 2 - Inter-VLAN Routing - Etherchannel - CDP - STP" and start your all your nodes. ![startnodes](/Day%202%20lab%202%20image001.png)
2. Verify CDP neighbors between all devices
3. Enable and verify LLDP on SW1 and SW2
4. Configure the port-description TLV on all switches **BONUS**
5. Disable the management-address TLV on all switches **BONUS**