# Day 2 Lab 5 STP Lab instructions
1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 2 - Inter-VLAN Routing - Etherchannel - CDP - STP" and start your all your nodes. ![startnodes](/Day%202%20lab%202%20image001.png)
2. Configure the "portfast" feature on all user facing ports
3. Determine the root bridge. Why was it chosen?
4. Which ports are shut down?
5. Set SW3 as the root bridge for all VLANs
6. Confirm desired end-state