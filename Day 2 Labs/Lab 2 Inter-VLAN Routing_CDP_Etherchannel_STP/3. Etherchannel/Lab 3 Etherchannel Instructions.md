# Day 2 Lab 4 Etherchannel Lab instructions

1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 2 - Inter-VLAN Routing - Etherchannel - CDP - STP" and start your all your nodes. ![startnodes](/Day%202%20lab%202%20image001.png)
2. Bundle SW1 eth1/0, eth1/1 into an etherchannel, mode **passive**
3. Bundle SW3 eth1/0, eth1/1 into an etherchannel mode **active**
4. Verify an LACP Etherchannel between SW1--->SW3
5. Configure the new port channel interface as a trunk on both sides
6. Bundle SW2 eth1/2, eth1/3 into an Etherchannel, mode **auto**
7. Bundle SW3 eth1/2, eth1/3 into an Etherchannel, mode **desirable**
8. Verify an PAgP Etherchannel between SW2-SW3
9. Configure the new port channel interface as a trunk on both sides
10. Verify full lab connectivity