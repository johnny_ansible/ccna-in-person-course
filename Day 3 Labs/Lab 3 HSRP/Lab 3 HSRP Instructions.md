# Day 3 Lab 3 HSRP Lab instructions
1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 4 - HSRP" and start your all your nodes. ![startnodes](/Day%203%20lab%203%20image001.png)
2. What addresses are the PC's currently using as the default gateway?
3. Configure "standby 100 ip 10.100.0.3" on each eth0/2.100 subinterface
4. Configure R1 to have the higher priority value for the ENGINEER network
5. Verfiy via show commands "show standby brief"
6. Configure "standby 200 ip 10.200.0.3" on each eth0/2.200 subinterface
7. Configure R2 to have the higher priority value for the sales network
8. Verify via show commands "show standby brief"
9. Change PC's default gateways to new virtual HSRP address
10. Start a continuous ping to google on all PC's ("ping google.com -t")
11. Shutdown one of the routers. Predict what will happen. Why?