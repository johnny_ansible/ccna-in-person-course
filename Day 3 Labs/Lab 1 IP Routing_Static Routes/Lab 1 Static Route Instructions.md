# Day 3 Lab 1 Static Routes Lab instructions
1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 3 - Routing - Static Routes - OSPF" and start your all your nodes. ![startnodes](/day%203%20lab%201%20image001.png)

## Part 1
**Planning**
1. Subnet for all VLANs at each location that needs it.
2. Record for all networks:
  Subnet ID, First Usable - Last Usable, Broadcast


**Layer2**
3. Create the VLANs needed on each switch
4. Assign access ports to the correct VLAN
5. Create trunk ports where needed
6. Verify VLANs and trunks via show commands

**Layer3**
7. Configure Router Sub-interfaces for each VLAN. Use first Usable.
8. Configure PC's with usable IP's within their subnet
9. Test reachability between each PC and its gateway (Router)

## Part 2
**WAN**
1. Turn on and IP all WAN connections
2. Test reachability within each WAN subnet

**PAUSE**
Why can't you ping from PC to PC across the WAN?

**Static Routes**
3. Configure each router to reach the destination networks using a static route
4. View the routing table via show commands
5. Test reachability. Can every PC ping every other PC?