# Day 3 Lab 2 OSPF Lab instructions
1. Login to EVE-NG and open up the **CCNA** folder and open "Lab 3 - Routing - Static Routes - OSPF" and start your all your nodes. ![startnodes](/day%203%20lab%201%20image001.png)

1. Delete all static routes
2. Turn on OSPF on the PHX, NY, and DC routers.
3. Advertise all directly connected networks on each router
4. Verify full OSPF adjacency between each router
5. Test reachability between PC's over the WAN
